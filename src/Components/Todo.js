import React, { useContext } from "react";
import Form from "./Form";
import Header from "./Header";
import Layout from "./Layout";
import Lists from "./Lists";
import { TaskListContext } from "../contexts/TaskListContext";
const Todo = () => {
  const {
    error,
    todo,
    todos,
    doneHandler,
    delHandler,
    submitHandler,
    handleChange,
  } = useContext(TaskListContext);

  return (
    <Layout>
      <Header />
      <Form
        error={error}
        value={todo}
        submit={submitHandler}
        onChange={handleChange}
      />
      <hr className="border-primary" />
      <Lists todos={todos} delHandler={delHandler} doneHandler={doneHandler} />
    </Layout>
  );
};

export default Todo;
