import { render, screen } from "@testing-library/react";
import Header from "./Header";

describe("Header", () => {
  it("same h3 element", () => {
    render(<Header title="Todo App - Build with ReactJS" />);
    const h3Element = screen.getByText(/Todo App - Build with ReactJS/i);
    expect(h3Element).toBeInTheDocument();
  });
});
