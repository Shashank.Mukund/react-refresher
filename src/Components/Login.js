import React, { useContext } from "react";
import { Alert } from "react-bootstrap";
import Layout from "./Layout";
import { Navigate } from "react-router-dom";
import { TaskListContext } from "../contexts/TaskListContext";

function Login() {
  const { flag, home, handleLogin, EmailogChange, PasswordlogChange } =
    useContext(TaskListContext);
  return (
    <Layout>
      <div>
        {home ? (
          <form onSubmit={handleLogin}>
            <h3>LogIn</h3>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                onChange={EmailogChange}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                onChange={PasswordlogChange}
              />
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block">
              Login
            </button>

            {flag && (
              <Alert color="primary" variant="warning">
                Fill correct Info else keep trying.
              </Alert>
            )}
          </form>
        ) : (
          <Navigate to="/Todo" />
        )}
      </div>
    </Layout>
  );
}

export default Login;
