import React, { createContext, useState, useEffect } from "react";

export const TaskListContext = createContext();
//Todo_Api
const TaskListContextProvider = (props) => {
  const [error, setError] = useState(null);
  const [todo, setTodo] = useState("");
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem("todos"));
    if (storedTodos) setTodos(storedTodos);
  }, []);

  // saving the todos in browser storage to prevent loss of todos on refreshing tab
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const submitHandler = (e) => {
    e.preventDefault();

    if (todo.length < 5) {
      setError("At least 5 word required!");
      return false;
    }

    setTodos([{ id: Date.now(), title: todo, done: false }, ...todos]);

    setTodo("");
    setError(null);
  };

  const delHandler = (todoId) => {
    if (window.confirm("Are you sure")) {
      const updatedTodos = todos.filter((item) => item.id !== todoId);

      setTodos(updatedTodos);
    }
  };

  const doneHandler = (todoId) => {
    const index = todos.findIndex((emp) => emp.id === todoId);
    const newTodo = [...todos];

    newTodo[index] = {
      id: todos[index].id,
      title: todos[index].title,
      done: !todos[index].done,
    };

    setTodos(newTodo);
  };
  const handleChange = (e) => {
    setTodo(e.target.value);
  };
  //Login_Api
  const [emaillog, setEmaillog] = useState(" ");
  const [passwordlog, setPasswordlog] = useState(" ");

  const [flag, setFlag] = useState(false);

  const [home, setHome] = useState(true);

  function handleLogin(e) {
    e.preventDefault();
    let pass = localStorage.getItem("givenPassword").replace(/"/g, "");
    let mail = localStorage.getItem("givenEmail").replace(/"/g, "");

    if (!emaillog || !passwordlog) {
      setFlag(true);
      console.log("EMPTY");
    } else if (passwordlog !== pass || emaillog !== mail) {
      setFlag(true);
    } else {
      setHome(!home);
      setFlag(false);
    }
  }
  const EmailogChange = (event) => {
    setEmaillog(event.target.value);
  };
  const PasswordlogChange = (event) => {
    setPasswordlog(event.target.value);
  };
  return (
    <TaskListContext.Provider
      value={{
        error,
        todo,
        todos,
        submitHandler,
        doneHandler,
        delHandler,
        handleChange,
        emaillog,
        passwordlog,
        flag,
        home,
        handleLogin,
        EmailogChange,
        PasswordlogChange,
      }}
    >
      {props.children}
    </TaskListContext.Provider>
  );
};

export default TaskListContextProvider;
