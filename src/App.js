import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Login from "./Components/Login";
import Todo from "./Components/Todo";
import TaskListContent from "../src/contexts/TaskListContext";

function App() {
  return (
    <TaskListContent>
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/todo" element={<Todo />} />
        </Routes>
      </Router>
    </TaskListContent>
  );
}

export default App;
